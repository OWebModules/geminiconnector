﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class IssueComment
    {
        public clsOntologyItem Commit { get; set; }
        public DateTime CreateDate { get; set; }
        public string Comment { get; set; }

        public string Url { get; set; }
    }
}
