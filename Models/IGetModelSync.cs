﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public interface IGetModelSync
    {
        string IdConfig { get; set; }
    }
}
