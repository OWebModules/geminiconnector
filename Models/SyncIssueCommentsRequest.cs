﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class SyncIssueCommentsRequest : IGetModelSync
    {
        public string IdConfig { get; set; }
        public List<IssueComment> IssueComments { get; set; }
        public string IdMasterUser { get; private set; }
        public string MasterPassword { get; private set; }

        public string IdUser { get; set; }

        public string IdGroup { get; set; }

        public SyncIssueCommentsRequest(string idConfig, List<IssueComment> issueComments, string idMasterUser, string masterPassword, string idUser, string idGroup)
        {
            IdConfig = idConfig;
            IssueComments = issueComments;
            IdUser = idUser;
            IdGroup = idGroup;
            IdMasterUser = idMasterUser;
            MasterPassword = masterPassword;
        }
    }
}
