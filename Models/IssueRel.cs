﻿using Countersoft.Gemini.Commons.Dto;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeManagementModule.Models;

namespace GeminiConnectorModule.Models
{
    public class IssueRel
    {
        public clsOntologyItem Issue { get; set; }
        public clsOntologyItem IssueKey { get; set; }

        public IssueDto GeminiIssue { get; set; }

        public List<IssueTimeTrackingDto> TimeTrackings { get; set; } = new List<IssueTimeTrackingDto>();

        public List<TimeTrackingItem> TimeManagementEntries { get; set; } = new List<TimeTrackingItem>();
        
    }

    public class TimeTrackingItem
    {
        public TimeManagementEntry TimeManagementEntry { get; set; }
        public TimeTracking TimeTracking { get; set; }
    }
}
