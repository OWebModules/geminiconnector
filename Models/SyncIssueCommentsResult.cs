﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class SyncIssueCommentsResult
    {
        public int CountSynced { get; set; }
    }
}
