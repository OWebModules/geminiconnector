﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class SyncIssuesRequest : IGetModelSync
    {
        public string IdConfig { get; set; }
        public string IdMasterUser { get; private set; }
        public string MasterPassword { get; private set; }
        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set;}

        public SyncIssuesRequest(string idConfig, string idMasterUser, string masterPassword)
        {
            IdConfig = idConfig;
            IdMasterUser = idMasterUser;
            MasterPassword = masterPassword;
        }
    }
}
