﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class SyncIssuesResult
    {
        public int CountFoundIssues { get; set; }
        public int CountSyncedIssues { get; set; }
        public List<int> ClosedIssueIds { get; set; } = new List<int>();

        public override string ToString()
        {
            var result = $"CountFoundIssues:{CountFoundIssues};CountSyncedIssues:{CountSyncedIssues};CountClosedIssues:{ClosedIssueIds.Count}";
            return result;
        }
    }
}
