﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Models
{
    public class GetSyncIssuesModel
    {
        public clsOntologyItem GeminiConnectorConfig { get; set; }
        public clsOntologyItem GeminiConnectorUrl { get; set; }
        public clsOntologyItem GeminiConnectorUser { get; set; }
        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }
        public clsObjectAtt SyncTimeEntries { get; set; }
    }
}
