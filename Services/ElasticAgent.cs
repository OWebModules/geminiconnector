﻿using Countersoft.Gemini.Commons.Dto;
using GeminiConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiConnectorModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<GetSyncIssuesModel>> GetSyncIssuesModel(IGetModelSync request)
        {
            var taskResult = await Task.Run<ResultItem<GetSyncIssuesModel>>(() =>
            {
                var result = new ResultItem<GetSyncIssuesModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetSyncIssuesModel()
                };

                if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Config-Id is not valid!";
                    return result;
                }

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.Result.GeminiConnectorConfig = dbReaderConfig.Objects1.FirstOrDefault();

                if (result.Result.GeminiConnectorConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Config found!";
                    return result;
                }

                var searchUser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.GeminiConnectorConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_GeminiConnectorModule_authorized_by_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_GeminiConnectorModule_authorized_by_user.ID_Class_Right
                    }
                };

                var dbReaderUser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the User!";
                    return result;
                }

                if (dbReaderUser.ObjectRels.Any())
                {
                    result.Result.GeminiConnectorUser = dbReaderUser.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).FirstOrDefault();
                }

                


                var searchUrl = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.GeminiConnectorConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_GeminiConnectorModule_uses_Url.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_GeminiConnectorModule_uses_Url.ID_Class_Right
                    }
                };

                var dbReaderUrl = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Url to Gemini!";
                    return result;
                }

                if (dbReaderUrl.ObjectRels.Count != 1)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "One Gemini-Url must be connected to config!";
                    return result;
                }

                result.Result.GeminiConnectorUrl = dbReaderUrl.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                var searchUserTime = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.GeminiConnectorConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_GeminiConnectorModule_needs_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_GeminiConnectorModule_needs_user.ID_Class_Right

                    }
                };

                var dbReaderUserTime = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUserTime.GetDataObjectRel(searchUserTime);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the User";
                    return result;
                }

                result.Result.UserItem = dbReaderUserTime.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.UserItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the User!";
                    return result;
                }

                var searchGroup = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.GeminiConnectorConfig.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_GeminiConnectorModule_needs_Group.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_GeminiConnectorModule_needs_Group.ID_Class_Right

                    }
                };

                var dbReaderGroup = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGroup.GetDataObjectRel(searchGroup);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Group";
                    return result;
                }

                result.Result.GroupItem = dbReaderGroup.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.GroupItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Group!";
                    return result;
                }

                var searchSyncTimeEntries = new List<clsObjectAtt> { new clsObjectAtt
                    {
                        ID_Object = result.Result.GeminiConnectorConfig.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Sync_Time_Entries.GUID
                    }
                };

                var dbReaderSynTimeEntries = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSynTimeEntries.GetDataObjectAtt(searchSyncTimeEntries);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sync Timeentries Flag!";
                    return result;
                }

                if (dbReaderSynTimeEntries.ObjAtts.Count != 1)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "There is no or more than one Timeentries Flag related! Only one is allowed!";
                    return result;
                }

                result.Result.SyncTimeEntries = dbReaderSynTimeEntries.ObjAtts.First();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetIssues(List<IssueDto> issues)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Error.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (!issues.Any()) return result;

                var searchIssues = issues.Select(issue => new clsObjectRel
                {
                    Name_Other = issue.IssueKey,
                    ID_Parent_Other = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Task_Id.ID_Class_Right,
                    ID_Parent_Object = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Task_Id.ID_Class_Left
                }).ToList();

                var dbReaderIssues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Issues";
                    return result;
                }

                result.Result = dbReaderIssues.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveIssues(List<IssueRel> issueRels)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var syncStamp = DateTime.Now;
                var syncItem = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = syncStamp.ToString("yyyy-MM-dd hhmmss"),
                    GUID_Parent = Config.LocalData.Class_Sync_Session__Gemini_.GUID,
                    Type = globals.Type_Object
                };

                var result = globals.LState_Success.Clone();
                var relationConfig = new clsRelationConfig(globals);

                var itemsToSave = issueRels.Where(issueRel => issueRel != null && issueRel.Issue.New_Item.Value).Select(issueRel => issueRel.Issue).ToList();
                itemsToSave.AddRange(issueRels.Where(issueRel => issueRel != null && issueRel.IssueKey != null && issueRel.IssueKey.New_Item.Value).Select(issueRel => issueRel.IssueKey));
                itemsToSave.Add(syncItem);

                var itemRelsToSave = issueRels.Where(issueRel => issueRel != null && issueRel.IssueKey != null && issueRel.Issue.New_Item.Value).Select(issueRel => relationConfig.Rel_ObjectRelation(issueRel.Issue, issueRel.IssueKey, Config.LocalData.RelationType_is_defined_by)).ToList();
                itemRelsToSave.AddRange(itemsToSave.Where(itm => itm.GUID_Parent == Config.LocalData.Class_Issue__Jira_.GUID).Select(itm => relationConfig.Rel_ObjectRelation(syncItem, itm, Config.LocalData.RelationType_contains)));

                var attributesToSave = new List<clsObjectAtt>
                {
                    relationConfig.Rel_ObjectAttribute(syncItem, Config.LocalData.AttributeType_DateTimestamp, syncStamp)
                };

                var dbWriter = new OntologyModDBConnector(globals);

                if (itemsToSave.Any())
                {
                    result = dbWriter.SaveObjects(itemsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Issues or Task-Ids";
                        return result;
                    }

                    result = dbWriter.SaveObjRel(itemRelsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the connection between Issues and Task-Ids";
                        return result;
                    }

                    result = dbWriter.SaveObjAtt(attributesToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the sync-state attribute";
                        return result;
                    }
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetObject(string idObject)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReaderObject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderObject.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idObject
                    }
                });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the object!";
                    return result;
                }

                result.Result = dbReaderObject.Objects1.FirstOrDefault();

                if (result.Result == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the object!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetIssueRelations(List<IssueDto> issues)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchIssuesIds = issues.Select(issue => new clsOntologyItem
                {
                    Name = issue.IssueKey,
                    GUID_Parent = Config.LocalData.Class_Task_Id.GUID
                }).ToList();

                if (!searchIssuesIds.Any()) return result;

                var dbReaderIssuesIds = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssuesIds.GetDataObjects(searchIssuesIds);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the issue-Ids!";
                    return result;
                }

                var searchIssues = dbReaderIssuesIds.Objects1.Select(id => new clsObjectRel
                {
                    ID_Parent_Object = Config.LocalData.Class_Issue__Jira_.GUID,
                    ID_RelationType = Config.LocalData.RelationType_is_defined_by.GUID,
                    ID_Other = id.GUID
                }).ToList();

                if (!searchIssues.Any()) return result;
                var dbReaderIssues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting relations between issue-Ids and issues!";
                    return result;
                }

                result.Result = dbReaderIssues.ObjectRels;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<TimeTracking>>> GetTimetracking(List<TimeManagementModule.Models.TimeManagementEntry> timeManagementEntries)
        {
            var taskResult = await Task.Run<ResultItem<List<TimeTracking>>>(() =>
            {
                var result = new ResultItem<List<TimeTracking>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<TimeTracking>()
                };

                var searchTimeTrackings = timeManagementEntries.Select(time => new clsObjectRel
                {
                    ID_Other = time.IdTimeManagement,
                    ID_RelationType = Config.LocalData.ClassRel_Timetracking_belongs_to_Timemanagement.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Timetracking_belongs_to_Timemanagement.ID_Class_Left
                }).ToList();

                var dbReaderTimeTrackings = new OntologyModDBConnector(globals);

                if (searchTimeTrackings.Any())
                {
                    result.ResultState = dbReaderTimeTrackings.GetDataObjectRel(searchTimeTrackings);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Timetrackings!";
                        return result;
                    }
                }

                var searchTimeTrackingToIssue = dbReaderTimeTrackings.ObjectRels.Select(timeTrack => new clsObjectRel
                {
                    ID_Object = timeTrack.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Timetracking_belongs_to_Issue__Jira_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Timetracking_belongs_to_Issue__Jira_.ID_Class_Right
                }).ToList();


                var dbReaderTimeTrackingsToIssues = new OntologyModDBConnector(globals);

                if (searchTimeTrackingToIssue.Any())
                {
                    result.ResultState = dbReaderTimeTrackingsToIssues.GetDataObjectRel(searchTimeTrackingToIssue);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Timetrackings to Issues!";
                        return result;
                    }
                }

                result.Result = (from timeTrackingToTimemanagement in dbReaderTimeTrackings.ObjectRels
                                 join timeTrackingToIssue in dbReaderTimeTrackingsToIssues.ObjectRels on timeTrackingToTimemanagement.ID_Object equals timeTrackingToIssue.ID_Object
                                 select new TimeTracking
                                 {
                                     IdTimeTracking = timeTrackingToTimemanagement.ID_Object,
                                     NameTimeTracking = timeTrackingToTimemanagement.Name_Object,
                                     IdTimeManagement = timeTrackingToTimemanagement.ID_Other,
                                     IdIssue = timeTrackingToIssue.ID_Other
                                 }).ToList();

                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
