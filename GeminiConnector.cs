﻿using Countersoft.Gemini.Api;
using Countersoft.Gemini.Commons.Dto;
using Countersoft.Gemini.Commons.Entity;
using GeminiConnectorModule.Models;
using GeminiConnectorModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TimeManagementModule;
using TimeManagementModule.Models;
using IssueComment = Countersoft.Gemini.Commons.Entity.IssueComment;

namespace GeminiConnectorModule
{
    public class GeminiConnector : AppController
    {
        public async Task<ResultItem<SyncIssuesResult>> SyncIssues(SyncIssuesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncIssuesResult>>(async () =>
           {
               request.MessageOutput?.OutputInfo("Start SyncIssues...");

               var result = new ResultItem<SyncIssuesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new SyncIssuesResult()
               };

               var serviceAgent = new ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo("Getting Model Start...");
               var serviceResultModel = await serviceAgent.GetSyncIssuesModel(request);
               request.MessageOutput?.OutputInfo("Getting Model Finished");

               result.ResultState = serviceResultModel.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (string.IsNullOrEmpty(request.IdMasterUser) || !Globals.is_GUID(request.IdMasterUser))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "MasterUser-Id is not valid!";
                   return result;
               }

               if (string.IsNullOrEmpty(request.MasterPassword))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You must provide a password!";
                   return result;
               }

               try
               {
                   var geminiUri = new Uri(serviceResultModel.Result.GeminiConnectorUrl.Name);
               }
               catch (Exception ex)
               {

                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Gemini-Url is not valid: {ex.Message}";
               }

               var password = "";
               if (serviceResultModel.Result.GeminiConnectorUser != null)
               {
                   var securityConntroller = new SecurityController(Globals);

                   request.MessageOutput?.OutputInfo($"Getting Password of user {serviceResultModel.Result.GeminiConnectorUser.Name} ...");
                   var getPasswordResult = await securityConntroller.GetPassword(serviceResultModel.Result.GeminiConnectorUser, request.MasterPassword);

                   result.ResultState = getPasswordResult.Result;
                   var credential = getPasswordResult.CredentialItems?.FirstOrDefault();
                   if (result.ResultState.GUID == Globals.LState_Error.GUID || credential == null)
                   {
                       result.ResultState.Additional1 = "No password for user found!";
                       request.MessageOutput?.OutputError($"No password for user {serviceResultModel.Result.GeminiConnectorUser.Name} found!");
                       return result;
                   }


                   password = credential.Password.Name_Other;
                   request.MessageOutput?.OutputInfo($"Getting Password of user {serviceResultModel.Result.GeminiConnectorUser.Name} finished");
               }
               

               var timeManagementController = new TimeManagementController(Globals);

               ResultItem<TimeManagementEntryList> timeManagementEntriesResult = null;

               var timeTrackings = new List<TimeTrackingItem>();
               var syncTimeEntries = serviceResultModel.Result.SyncTimeEntries != null && serviceResultModel.Result.SyncTimeEntries.Val_Bit.HasValue ? serviceResultModel.Result.SyncTimeEntries.Val_Bit.Value : false;
               request.MessageOutput?.OutputInfo($"Sync Timeentries: {syncTimeEntries}");
               if (syncTimeEntries)
               {
                   timeManagementEntriesResult = await timeManagementController.GetTimeManagementEntryList(serviceResultModel.Result.UserItem, serviceResultModel.Result.GroupItem);

                   result.ResultState = timeManagementEntriesResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the TimemanagementEntries!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var timeManagementEntries = timeManagementEntriesResult.Result.EntryList;

                   var timeTrackingsResult = await serviceAgent.GetTimetracking(timeManagementEntries);

                   result.ResultState = timeTrackingsResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   timeTrackings = (from timeManagementEntry in timeManagementEntries
                                    join timeTracking in timeTrackingsResult.Result on timeManagementEntry.IdTimeManagement equals timeTracking.IdTimeManagement into timeTrackings1
                                    from timeTracking in timeTrackings1.DefaultIfEmpty()
                                    select new TimeTrackingItem { TimeManagementEntry = timeManagementEntry, TimeTracking = timeTracking }).ToList();

                   request.MessageOutput?.OutputInfo($"Found {timeTrackings.Count} Timetrackings");
               }
               
               ServiceManager geminiLogin;
               UserDto user;
               int statusIdClosed = 0;
               var geminiItems = new List<IssueDto>();
               try
               {
                   if (serviceResultModel.Result.GeminiConnectorUser != null)
                   {
                       geminiLogin = new ServiceManager(serviceResultModel.Result.GeminiConnectorUrl.Name, serviceResultModel.Result.GeminiConnectorUser.Name, password, "", false);
                   }
                   else
                   {
                       geminiLogin = new ServiceManager(serviceResultModel.Result.GeminiConnectorUrl.Name);
                   }
                   
                   user = geminiLogin.Admin.WhoAmI();
                   request.MessageOutput?.OutputInfo($"Gemini-User: {user.Fullname}");

                   geminiItems = geminiLogin.Item.GetFilteredItems(new Countersoft.Gemini.Commons.Entity.IssuesFilter
                   {
                       IncludeClosed = true,
                       Resources = user.Entity.Id.ToString(),
                       MaxItemsToReturn = -1
                   });

                   var geminiItemsToClose = geminiItems.Where(item => item.IsClosed).ToList();

                   if (geminiItemsToClose.Any())
                   {
                       statusIdClosed = geminiItemsToClose.First().Entity.StatusId;
                   }

               }
               catch (Exception ex)
               {

                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while login to Gemini: {ex.Message}";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }


               var issuesResult = await serviceAgent.GetIssues(geminiItems);

               var issuesToSave = (from geminiItem in geminiItems
                                   join issue in issuesResult.Result on geminiItem.IssueKey equals issue.Name_Other into issues
                                   from issue in issues.DefaultIfEmpty()
                                   select new { geminiItem, issue }).Select(issueRel =>
                                   {
                                       if (issueRel.issue == null)
                                       {
                                           var issueRelItem = new IssueRel
                                           {
                                               Issue = new OntologyClasses.BaseClasses.clsOntologyItem
                                               {
                                                   GUID = Globals.NewGUID,
                                                   Name = issueRel.geminiItem.Title,
                                                   GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                                                   Type = Globals.Type_Object,
                                                   New_Item = true
                                               },
                                               IssueKey = new OntologyClasses.BaseClasses.clsOntologyItem
                                               {
                                                   GUID = Globals.NewGUID,
                                                   Name = issueRel.geminiItem.IssueKey,
                                                   GUID_Parent = Config.LocalData.Class_Task_Id.GUID,
                                                   Type = Globals.Type_Object,
                                                   New_Item = true
                                               },
                                               GeminiIssue = issueRel.geminiItem,
                                               TimeTrackings = issueRel.geminiItem.TimeEntries,
                                               TimeManagementEntries = syncTimeEntries && issueRel.issue != null ? timeTrackings.Where(time => time.TimeManagementEntry.IdReference == issueRel.issue.ID_Object || time.TimeManagementEntry.IdReference == issueRel.issue.ID_Other).ToList() : new List<TimeTrackingItem>()
                                           };
                                           return issueRelItem;
                                       }
                                       else if (issueRel.geminiItem.Entity.Title != issueRel.issue.Name_Object)
                                       {
                                           var issueRelItem = new IssueRel
                                           {
                                               Issue = new OntologyClasses.BaseClasses.clsOntologyItem
                                               {
                                                   GUID = issueRel.issue.ID_Object,
                                                   Name = issueRel.geminiItem.Title,
                                                   GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                                                   Type = Globals.Type_Object,
                                                   New_Item = false
                                               },
                                               GeminiIssue = issueRel.geminiItem,
                                               TimeTrackings = issueRel.geminiItem.TimeEntries,
                                               TimeManagementEntries = syncTimeEntries ? timeTrackings.Where(time => time.TimeManagementEntry.IdReference == issueRel.issue.ID_Object || time.TimeManagementEntry.IdReference == issueRel.issue.ID_Other).ToList(): new List<TimeTrackingItem>()
                                           };

                                           return issueRelItem;
                                       }
                                       else
                                       {
                                           return new IssueRel
                                           {
                                               Issue = new OntologyClasses.BaseClasses.clsOntologyItem
                                               {
                                                   GUID = issueRel.issue.ID_Object,
                                                   Name = issueRel.geminiItem.Title,
                                                   GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                                                   Type = Globals.Type_Object,
                                                   New_Item = false
                                               },
                                               IssueKey = new OntologyClasses.BaseClasses.clsOntologyItem
                                               {
                                                   GUID = issueRel.issue.ID_Other,
                                                   Name = issueRel.geminiItem.IssueKey,
                                                   GUID_Parent = Config.LocalData.Class_Task_Id.GUID,
                                                   Type = Globals.Type_Object,
                                                   New_Item = false
                                               },
                                               GeminiIssue = issueRel.geminiItem,
                                               TimeTrackings = issueRel.geminiItem.TimeEntries,
                                               TimeManagementEntries = syncTimeEntries ? timeTrackings.Where(time => time.TimeManagementEntry.IdReference == issueRel.issue.ID_Object || time.TimeManagementEntry.IdReference == issueRel.issue.ID_Other).ToList() : new List<TimeTrackingItem>()
                                           };
                                       }
                                   }).Where(issueRel => issueRel != null).ToList();

               var saveResult = await serviceAgent.SaveIssues(issuesToSave);

               result.ResultState = saveResult;
               request.MessageOutput?.OutputInfo($"Found {result.Result.CountFoundIssues} Issues");
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               

               if (syncTimeEntries)
               {
                   var relationConfig = new clsRelationConfig(Globals);
                   var transaction = new clsTransaction(Globals);

                   result.Result.ClosedIssueIds = new List<int>();
                   var stateIdClose = 0;
                   foreach (var issueToSave in issuesToSave)
                   {
                       var isClosed = issueToSave.GeminiIssue.IsClosed;
                       var timeentries = issueToSave.TimeManagementEntries.Where(timeEntry => timeEntry.TimeTracking == null);
                       foreach (var time in timeentries)
                       {
                           if (issueToSave.GeminiIssue.IsClosed)
                           {
                               stateIdClose = issueToSave.GeminiIssue.Entity.StatusId;
                               result.Result.ClosedIssueIds.Add(issueToSave.GeminiIssue.Id);
                           }

                           var timeSpan = time.TimeManagementEntry.Ende.Subtract(time.TimeManagementEntry.Start);


                           var issueTimeTracking = new IssueTimeTracking();


                           issueTimeTracking.EntryDate = time.TimeManagementEntry.Start;
                           issueTimeTracking.ProjectId = issueToSave.GeminiIssue.Project.Id;
                           issueTimeTracking.IssueId = issueToSave.GeminiIssue.Id;
                           issueTimeTracking.UserId = user.Entity.Id;
                           issueTimeTracking.Comment = HttpUtility.HtmlEncode(time.TimeManagementEntry.NameTimeManagement);
                           issueTimeTracking.Hours = timeSpan.Hours;
                           issueTimeTracking.Minutes = timeSpan.Minutes;


                           var saveTimeResult = geminiLogin.Item.LogTime(issueTimeTracking);
                           var timeTracking = new clsOntologyItem
                           {
                               GUID = Globals.NewGUID,
                               Name = $"{saveTimeResult.Entity.ProjectId.ToString()}_{saveTimeResult.Entity.IssueId.ToString()}_{saveTimeResult.Entity.Id}",
                               GUID_Parent = Config.LocalData.Class_Timetracking.GUID,
                               Type = Globals.Type_Object
                           };

                           transaction.ClearItems();
                           result.ResultState = transaction.do_Transaction(timeTracking);

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               result.ResultState.Additional1 = $"Error while saving the Tracking-Item {time.TimeManagementEntry.IdTimeManagement} - {time.TimeManagementEntry.NameTimeManagement}";
                               return result;
                           }


                           var timeTrackingToIssue = relationConfig.Rel_ObjectRelation(timeTracking, issueToSave.Issue, Config.LocalData.RelationType_belongs_to);
                           result.ResultState = transaction.do_Transaction(timeTrackingToIssue);

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               result.ResultState.Additional1 = $"Error while saving the Tracking-Item {time.TimeManagementEntry.IdTimeManagement} - {time.TimeManagementEntry.NameTimeManagement} to Issue";
                               transaction.rollback();
                               return result;
                           }

                           var timetrackingToTimemanagementEntry = relationConfig.Rel_ObjectRelation(timeTracking, new clsOntologyItem
                           {
                               GUID = time.TimeManagementEntry.IdTimeManagement,
                               Name = time.TimeManagementEntry.NameTimeManagement,
                               GUID_Parent = timeManagementController.ClassTimeManagement.GUID,
                               Type = Globals.Type_Object
                           }, Config.LocalData.RelationType_belongs_to);
                           result.ResultState = transaction.do_Transaction(timetrackingToTimemanagementEntry);

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               result.ResultState.Additional1 = $"Error while saving the Tracking-Item {time.TimeManagementEntry.IdTimeManagement} - {time.TimeManagementEntry.NameTimeManagement} to TimemanagementEntry";
                               transaction.rollback();
                               return result;
                           }

                       };

                       if (isClosed && timeentries.Any())
                       {
                           var issue = geminiItems.FirstOrDefault(item => item.Id == issueToSave.GeminiIssue.Id);

                           var entity = new IssueDto(issue);
                           entity.Entity.StatusId = statusIdClosed;
                           geminiLogin.Item.Update(entity.Entity);
                       }
                   }
               }
               
               

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<SyncIssueCommentsResult>> SyncSourceCodeComments(SyncIssueCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncIssueCommentsResult>>(async () =>
            {
                var result = new ResultItem<SyncIssueCommentsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncIssueCommentsResult()
                };

                var serviceAgent = new ElasticAgent(Globals);
                var serviceResultModel = await serviceAgent.GetSyncIssuesModel(request);

                result.ResultState = serviceResultModel.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (string.IsNullOrEmpty(request.IdMasterUser) || !Globals.is_GUID(request.IdMasterUser))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "MasterUser-Id is not valid!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.MasterPassword))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You must provide a password!";
                    return result;
                }

                try
                {
                    var geminiUri = new Uri(serviceResultModel.Result.GeminiConnectorUrl.Name);
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Gemini-Url is not valid: {ex.Message}";
                }


                var securityConntroller = new SecurityController(Globals);

                var password = string.Empty;
                if (serviceResultModel.Result.GeminiConnectorUser != null)
                {
                    var getPasswordResult = await securityConntroller.GetPassword(serviceResultModel.Result.GeminiConnectorUser, request.MasterPassword);

                    result.ResultState = getPasswordResult.Result;
                    var credential = getPasswordResult.CredentialItems?.FirstOrDefault();
                    if (result.ResultState.GUID == Globals.LState_Error.GUID || credential == null)
                    {
                        result.ResultState.Additional1 = "No password for user found!";
                        return result;
                    }


                    password = credential.Password.Name_Other;
                }
                

                var getUserResult = await serviceAgent.GetObject(request.IdUser);
                result.ResultState = getUserResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the user!";
                    return result;
                }

                var getGroupResult = await serviceAgent.GetObject(request.IdGroup);
                result.ResultState = getGroupResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the group!";
                    return result;
                }

                try
                {
                    ServiceManager geminiLogin = null;
                    UserDto user;
                    var relationConfig = new clsRelationConfig(Globals);
                    var geminiItems = new List<IssueDto>();
                    var geminiItemIdsToClose = new List<int>();
                    var statusIdClosed = 0;
                    try
                    {
                        if (serviceResultModel.Result.GeminiConnectorUser != null)
                        {
                            geminiLogin = new ServiceManager(serviceResultModel.Result.GeminiConnectorUrl.Name, serviceResultModel.Result.GeminiConnectorUser.Name, password, "", false);
                        }
                        else
                        {
                            geminiLogin = new ServiceManager(serviceResultModel.Result.GeminiConnectorUrl.Name);
                        }
                        
                        user = geminiLogin.Admin.WhoAmI();

                        geminiItems = geminiLogin.Item.GetFilteredItems(new Countersoft.Gemini.Commons.Entity.IssuesFilter
                        {
                            IncludeClosed = true,
                            Resources = user.Entity.Id.ToString()
                        });

                        var geminiItemsToClose = geminiItems.Where(item => item.IsClosed).ToList();
                        geminiItemIdsToClose = geminiItemsToClose.Select(item => item.Id).ToList();
                        
                        if (geminiItemsToClose.Any())
                        {
                            statusIdClosed = geminiItemsToClose.First().Entity.StatusId;
                        }
                    }
                    catch (Exception ex)
                    {

                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Error while login to Gemini: {ex.Message}";
                        return result;
                    }

                    var issuesResult = await serviceAgent.GetIssueRelations(geminiItems);

                    var typedTaggingController = new TypedTaggingModule.Connectors.TypedTaggingConnector(Globals);

                    foreach (var issueComment in request.IssueComments.OrderBy(comment => comment.CreateDate))
                    {
                        var comment = $"{issueComment.CreateDate} - TFS Changeset {issueComment.Commit.Name} - {issueComment.Comment}";
                        var checkComment = $"TFS Changeset {issueComment.Commit.Name}";
                        var task = geminiItems.FirstOrDefault(tsk => issueComment.Comment.Contains($"#{tsk.IssueKey}"));

                        if (task != null)
                        {
                            var isClosed = task.IsClosed;
                            var issueRel = issuesResult.Result.FirstOrDefault(rel => rel.Name_Other == task.IssueKey);
                            if (issueRel == null)
                            {
                                result.ResultState = Globals.LState_Error.Clone();
                                result.ResultState.Additional1 = $"The issue of task {task.Id} cannot be found!";
                                return result;
                            }
                            try
                            {
                                var comments = task.Comments;
                                if (!comments.Any(com => com.Entity.Comment.Contains(checkComment)))
                                {
                                    var newComment = new IssueComment
                                    {
                                        Comment = comment,
                                        IssueId = task.Id,
                                        Created = issueComment.CreateDate,
                                        Revised = issueComment.CreateDate,
                                    };
                                    
                                    //var newComment = new Countersoft.Gemini.Commons.Entity.CodeCommit
                                    //{
                                    //    Comment = comment,
                                    //    IssueId = task.Id,
                                    //    Created = issueComment.CreateDate,
                                    //    Revised = issueComment.CreateDate,
                                    //    Provider = Countersoft.Gemini.Commons.SourceControlProvider.TFS2012
                                    //};

                                    geminiLogin.Item.IssueCommentCreate(newComment);

                                    var typedTagResult = await typedTaggingController.SaveTags(issueComment.Commit, new List<clsOntologyItem>
                                   {
                                       new clsOntologyItem
                                       {
                                           GUID = issueRel.ID_Object,
                                           Name = issueRel.Name_Object,
                                           GUID_Parent = issueRel.ID_Parent_Object,
                                           Type = Globals.Type_Object
                                       }
                                   },
                                    getUserResult.Result,
                                    getGroupResult.Result, false, null);

                                    result.ResultState = typedTagResult.Result;

                                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                    {
                                        return result;
                                    }

                                    if (isClosed)
                                    {
                                        var issue = geminiItems.FirstOrDefault(item => item.Id == task.Id);

                                        var entity = new IssueDto(issue);
                                        entity.Entity.StatusId = statusIdClosed;
                                        geminiLogin.Item.Update(entity.Entity);
                                    }
                                    
                                }
                            }
                            catch (Exception ex)
                            {


                            }

                        }
                    }

                    

                    

                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while connecting to Wrike: {ex.Message}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public GeminiConnector(Globals globals) : base(globals)
        {
        }
    }
}
